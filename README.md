[![Build Status](https://gitlab.com/meltano/hub/badges/main/pipeline.svg)](https://gitlab.com/meltano/hub/-/pipelines?ref=main)

---

## MeltanoHub

Source of MeltanoHub <https://hub.meltano.com/>.

## Development

To work locally with this project, you'll have to follow the steps below:

1. Fork this project
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Add content
1. Push the commit(s) you made
1. Make an MR

The above commands should be executed from the root directory of this project.

Read more at Jekyll's [documentation][].

[documentation]: https://jekyllrb.com/docs/home/
